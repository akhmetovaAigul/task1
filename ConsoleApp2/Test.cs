﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NLog;

namespace ConsoleApp2
{
    public class Test
    {
        
        public List<string> Files = new List<string>();
        public List<string> FilesResult = new List<string>();
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public void FindMethod(string pathString, string strMethod)
        {
            if (pathString == null) return;
            var pathArray = pathString.Split(';', ',');
            foreach (var item in pathArray)
                GetFiles(item, strMethod);
        }


        public IEnumerable<string> GetFiles(string path, string strMethod)
        {
            var foundFiles = Enumerable.Empty<string>();
           
            try
            {
                IEnumerable<string> subDirs = Directory.EnumerateDirectories(path);

                foreach (var directory in subDirs)
                {
                    Console.WriteLine("найден каталог" + directory);
                    foundFiles = foundFiles.Concat(GetFiles(directory, strMethod));
                }
            }
            catch (UnauthorizedAccessException e) {  }
            catch (PathTooLongException e ) {  }

            try
            {
                foreach (var item  in Directory.EnumerateFiles(path, ".").Where(file => file.EndsWith(".exe") || file.EndsWith(".dll")))
                {
                    Console.WriteLine("найден файл " + item);
                    SearchMethod(item, strMethod);
                }
            }
            catch (UnauthorizedAccessException) { }
            Console.WriteLine("это не конец");
            return foundFiles;
        }

        private void SearchMethod(string pathFile, string strMethod)
        {
            try
            {
                var asm = Assembly.LoadFrom(pathFile);
                var types = asm.GetTypes();
                foreach (var t in types)
                {
                    foreach (var method in t.GetMethods())
                    {
                        if (method.Name == strMethod && method.IsPublic)
                        {
                            FilesResult.Add(t.Module + ":" + t.FullName + " " + method.Name);
                        }

                    }
                }

            }
            catch (Exception)
            {
                
            }
            

        }

    }
}